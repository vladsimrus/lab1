#include <iostream>
#include "iterator.h"

using namespace std;

int main()
{
    Iterator a("C:\\Games", "text.txt");
    FileItem *b;

    while (a.hasMore()){
        b = a.next();
        std::cout << b->fname.c_str() << "  ::  " << b->fMask.c_str() << "\n";
		delete b;
    }

    return 0;
}
